//
//  Driver2.m
//  ho-assign11
//
//  Created by Henrique de Oliveira Carvalho on 2014-11-26.
//  Copyright (c) 2014 beta. All rights reserved.
//
// Problem Statement 2: Develop an application that meets the following requirements:
// - Create a function reverseAndCapitalize that returns a reversed NSString with
// the first character properly capitalized. Create a function alternateCases
// that returns an NSString where each character at the odd position is an upper
// case character and each character at the even position is a lower case character.
// - Create a function eraseDigits that returns an NSString without any digit
// (i.e. if the NSString was "I was 10 years old", it should return "I was years old").
// - Create a function getMD5 that returns the MD5 cryptograhic message digest
// of a given NSString.
// - Create a function listAllFilesAndFolders that returns an NSMutableArray
// containing names of all files and folders in the present directory (i.e. where
// this program runs).
// - Create a function allPossibleStrings that returns an NSMutableArray with all
// possible permutation of a given string. If, for example, the given string is
// "Abc", the output should be an NSMutableArray containing  6 strings: "Abc",
// "Acb", "bAc", "bcA", "cAb", and "cbA".
// Your Driver2.m should contain a code that demonstrates all developed functions.
//
// Inputs:   none
// Outputs:  none
//
// ******************************************************************************

#import "Driver2.h"
#import "NSString+NSString_ExtraStringOperations.h"

@implementation Driver2

- (void) run
{
  NSString * stringTest = @"hello";

  NSLog(@"%@", [stringTest reverseAndCapitalize:stringTest]);

  stringTest = @"Beta Tests";
  NSLog(@"%@", [stringTest alternateCases:stringTest]);

  stringTest = @"he12ll34";
  NSLog(@"%@", [stringTest eraseDigits:stringTest]);

  stringTest = @"hello";
  NSLog(@"%@", [stringTest getMd5:stringTest]);

  NSLog(@"%@", [stringTest listAllFilesAndFolders]);

}

@end

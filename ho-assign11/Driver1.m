//
//  Driver1.m
//  ho-assign11
//
//  Created by Henrique de Oliveira Carvalho on 2014-11-26.
//  Copyright (c) 2014 beta. All rights reserved.
//
// Problem Statement 1: The main application class is the PrinterCenter class
// that contains a delegate printer. At any time, you can assign a printer
// object that implements the PrinterProtocol to the PrinterCenter and start
// printing. Your task is to modify this applications so that it meets the
// following requirements:
// - Develop additional 2 printer classes. Be sure that the new printer classes
// differ and that not all of them implement the @optional functions of the
// PrinterProtocol.
// - Modify the PrinterCenter class so that it contains an NSMutableArray of
// printers, i.e. you can add as many printers (provided they implement the
// PrinterProtocol) as you want - at least 6.
// - Modify the function printGivenJob so that a given print job is printed by
// a randomly chosen printer. Be sure that the printGivenJob function first tests
// if the randomly selected printer has implemented the necessary functions.If
// not print a warning message.
// Your Driver1.m should contain a loop that prints at least 10 different
// NSString messages (i.e. 10 different print jobs).
//
// Inputs:   none
// Outputs:  none
//

#import "Driver1.h"
#import "PrinterCenter.h"
#import "LaserPrinter.h"
#import "LinePrinter.h"
#import "PrinterCenter.h"
#import "PrinterOne.h"
#import "PrinterSecond.h"

@implementation Driver1
- (void) run
{
  PrinterCenter * printers = [[PrinterCenter alloc] init];

  // create 2 Laser Printers
  LaserPrinter * laser = [[LaserPrinter alloc] init];

  [laser setLocation:@"Tyee Hall"];

  LaserPrinter * superLaser = [[LaserPrinter alloc] init];
  [superLaser setLocation:@"Discovery Hall"];

  // create 2 old Line Printers
  LinePrinter * slowLinePrinter = [[LinePrinter alloc] init];
  [slowLinePrinter setLocation:@"COMOX Hall"];

  LinePrinter * linePrinter = [[LinePrinter alloc] init];
  [linePrinter setLocation:@"COURTENAY Hall"];

  // new classes printer
  PrinterOne * printerOneTest = [[PrinterOne alloc] init];
  [printerOneTest setLocation:@"TEST ONE Hall"];

  PrinterSecond * printerSecondTest = [[PrinterSecond alloc] init];
  [printerSecondTest setLocation:@"TEST SECOND Hall"];

  [printers addPrinter:laser];
  [printers addPrinter:superLaser];
  [printers addPrinter:slowLinePrinter];
  [printers addPrinter:linePrinter];
  [printers addPrinter:printerOneTest];
  [printers addPrinter:printerSecondTest];

  NSString * strAux = @"";

  for (int count = 0; count < 10; count++)
  {
    strAux = [strAux stringByAppendingFormat:@"%d", count];
    [printers printGivenJob:@"%d EXAMS"];
  }

}
@end

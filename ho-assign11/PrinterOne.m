//
//  PrinterOne.m
//  ho-assign11
//
//  Created by Henrique de Oliveira Carvalho on 2014-11-25.
//  Copyright (c) 2014 beta. All rights reserved.
//
// Problem Statement 1: The main application class is the PrinterCenter class
// that contains a delegate printer. At any time, you can assign a printer
// object that implements the PrinterProtocol to the PrinterCenter and start
// printing. Your task is to modify this applications so that it meets the
// following requirements:
// - Develop additional 2 printer classes. Be sure that the new printer classes
// differ and that not all of them implement the @optional functions of the
// PrinterProtocol.
// - Modify the PrinterCenter class so that it contains an NSMutableArray of
// printers, i.e. you can add as many printers (provided they implement the
// PrinterProtocol) as you want - at least 6.
// - Modify the function printGivenJob so that a given print job is printed by
// a randomly chosen printer. Be sure that the printGivenJob function first tests
// if the randomly selected printer has implemented the necessary functions.If
// not print a warning message.
// Your Driver1.m should contain a loop that prints at least 10 different
// NSString messages (i.e. 10 different print jobs).
//
// Inputs:   none
// Outputs:  none
//

#import "PrinterOne.h"

@implementation PrinterOne

- (BOOL) printMaterial:(NSString *)work
{
  [self setMaterial:work];
  NSLog(@"============ Printer One START   ");
  NSLog(@" Printed Job: %@", work);
  NSLog(@"============ Printer One STOP   ");
  return TRUE;
}

- (NSString *) printerLocation
{
  return [self location];
}

- (id) init
{
  self = [super init];
  if (self)
  {
    [self setMaterial:@"" ];
    [self setLocation:@"" ];
  }
  return self;
}

@end
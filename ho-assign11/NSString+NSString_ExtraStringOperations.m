//
//  NSString+NSString_ExtraStringOperations.m
//  ho-assign11
//
//  Created by Henrique de Oliveira Carvalho on 2014-11-26.
//  Copyright (c) 2014 beta. All rights reserved.
//
// Problem Statement 2: Develop an application that meets the following requirements:
// - Create a function reverseAndCapitalize that returns a reversed NSString with
// the first character properly capitalized. Create a function alternateCases
// that returns an NSString where each character at the odd position is an upper
// case character and each character at the even position is a lower case character.
// - Create a function eraseDigits that returns an NSString without any digit
// (i.e. if the NSString was "I was 10 years old", it should return "I was years old").
// - Create a function getMD5 that returns the MD5 cryptograhic message digest
// of a given NSString.
// - Create a function listAllFilesAndFolders that returns an NSMutableArray
// containing names of all files and folders in the present directory (i.e. where
// this program runs).
// - Create a function allPossibleStrings that returns an NSMutableArray with all
// possible permutation of a given string. If, for example, the given string is
// "Abc", the output should be an NSMutableArray containing  6 strings: "Abc",
// "Acb", "bAc", "bcA", "cAb", and "cbA".
// Your Driver2.m should contain a code that demonstrates all developed functions.
//
// Inputs:   none
// Outputs:  none
//
// ******************************************************************************


#import <CommonCrypto/CommonDigest.h>
#import "NSString+NSString_ExtraStringOperations.h"

@implementation NSString (NSString_ExtraStringOperations)

- (NSString *) reverseAndCapitalize:(NSString *)string
{

  NSMutableString * reversedStr;

  NSUInteger size = [string length];

  // auto released string
  reversedStr = [NSMutableString stringWithCapacity:size];

  // quick-and-dirty implementation
  while ( size > 0 )
  {
    [reversedStr appendString:[NSString stringWithFormat:@"%C", [string characterAtIndex:--size]]];
  }

  return [reversedStr capitalizedString];
}

- (NSString *) alternateCases:(NSString *)string
{

  NSString * strAux = @"";

  for (int charIndex = 0; charIndex < [string length]; charIndex++)
  {
    NSString * myChar = [NSString stringWithFormat:@"%c", [string characterAtIndex:charIndex]];

    if ((charIndex % 2) == 0)
    {
      myChar = [myChar lowercaseString];
    }
    else
    {
      myChar = [myChar uppercaseString];
    }

    strAux = [strAux stringByAppendingString:myChar];
  }

  return strAux;

}

- (NSString *) eraseDigits:(NSString *)string
{
  return [string stringByReplacingOccurrencesOfString:@"[^A-Za-z]" withString:@" "];
}

- (NSString *) getMd5:(NSString *)input
{
  const char * cStr = [input UTF8String];
  unsigned char digest[16];

  CC_MD5(cStr, strlen(cStr), digest);     // This is the md5 call

  NSMutableString * output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];

  for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
  {
    [output appendFormat:@"%02x", digest[i]];
  }

  return output;
}

- (NSMutableArray *) listAllFilesAndFolders
{
  NSString * sourcePath = @"ho-assign11";
  NSArray * dirs = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:sourcePath error:NULL];

  NSMutableArray * files = [[NSMutableArray alloc] init];

  [dirs enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * stop) {
     NSString * filename = (NSString *) obj;
     NSString * extension = [[filename pathExtension] lowercaseString];

     [files addObject:[sourcePath stringByAppendingPathComponent:filename]];

   }];

  return files;
}

@end

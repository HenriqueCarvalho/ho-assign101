//
//  main.m
//  DelegateProject
//
//  Created by Frank Niscak on 10/27/2013.
//  Copyright (c) 2013 Frank Niscak. All rights reserved.
//
//
// Problem Statement 1: The main application class is the PrinterCenter class
// that contains a delegate printer. At any time, you can assign a printer
// object that implements the PrinterProtocol to the PrinterCenter and start
// printing. Your task is to modify this applications so that it meets the
// following requirements:
// - Develop additional 2 printer classes. Be sure that the new printer classes
// differ and that not all of them implement the @optional functions of the
// PrinterProtocol.
// - Modify the PrinterCenter class so that it contains an NSMutableArray of
// printers, i.e. you can add as many printers (provided they implement the
// PrinterProtocol) as you want - at least 6.
// - Modify the function printGivenJob so that a given print job is printed by
// a randomly chosen printer. Be sure that the printGivenJob function first tests
// if the randomly selected printer has implemented the necessary functions.If
// not print a warning message.
// Your Driver1.m should contain a loop that prints at least 10 different
// NSString messages (i.e. 10 different print jobs).
//
// Inputs:   none
// Outputs:  none
//
// Problem Statement 2: Develop an application that meets the following requirements:
// - Create a function reverseAndCapitalize that returns a reversed NSString with
// the first character properly capitalized. Create a function alternateCases
// that returns an NSString where each character at the odd position is an upper
// case character and each character at the even position is a lower case character.
// - Create a function eraseDigits that returns an NSString without any digit
// (i.e. if the NSString was "I was 10 years old", it should return "I was years old").
// - Create a function getMD5 that returns the MD5 cryptograhic message digest
// of a given NSString.
// - Create a function listAllFilesAndFolders that returns an NSMutableArray
// containing names of all files and folders in the present directory (i.e. where
// this program runs).
// - Create a function allPossibleStrings that returns an NSMutableArray with all
// possible permutation of a given string. If, for example, the given string is
// "Abc", the output should be an NSMutableArray containing  6 strings: "Abc",
// "Acb", "bAc", "bcA", "cAb", and "cbA".
// Your Driver2.m should contain a code that demonstrates all developed functions.
//
// Inputs:   none
// Outputs:  none
//
// ******************************************************************************

#import <Foundation/Foundation.h>
#import "Driver1.h"
#import "Driver2.h"

int main(int argc, const char * argv[])
{

  @autoreleasepool {

    Driver1 * driver1 = [[Driver1 alloc] init];
    [driver1 run];

    Driver2 * driver2 = [[Driver2 alloc] init];
    [driver2 run];


  }
  return 0;
}
